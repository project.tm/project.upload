<?php

use Bitrix\Main\ModuleManager,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader,
    Project\Upload\Model\ImportTable,
    Project\Upload\Model\SyncTable;

IncludeModuleLangFile(__FILE__);

class project_upload extends CModule {

    var $MODULE_ID = 'project.upload';

    function __construct() {
        $arModuleVersion = array();

        include(__DIR__ . '/version.php');

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = Loc::getMessage('PROJECT_UPLOAD_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PROJECT_UPLOAD_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('PROJECT_UPLOAD_PARTNER_NAME');
        $this->PARTNER_URI = '';
    }

    public function DoInstall() {
        ModuleManager::registerModule($this->MODULE_ID);
        Loader::includeModule($this->MODULE_ID);
        $this->InstallAgent();
        $this->InstallDB();
    }

    public function DoUninstall() {
        Loader::includeModule($this->MODULE_ID);
        $this->UnInstallAgent();
        $this->UnInstallDB();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    /*
     * InstallAgent
     */

    public function InstallAgent() {
        $agent = '\Project\Upload\Event\Router::process();';
        $res = CAgent::GetList(Array(), Array("NAME" => $agent));
        if (!$res->Fetch()) {
            CAgent::AddAgent(
                    $agent, // имя функции
                    $this->MODULE_ID, // идентификатор модуля
                    "N", // агент не критичен к кол-ву запусков
                    30 * 60, // интервал запуска - 1 сутки
                    "", // дата первой проверки - текущее
                    "N", // агент активен
                    "", // дата первого запуска - текущее
                    30);
        }
    }

    public function UnInstallAgent() {
        CAgent::RemoveAgent('\Project\Upload\Event\Router::process();', $this->MODULE_ID);
    }

    /*
     * InstallDB
     */

    public function InstallDB() {
        ImportTable::tableCreate();
        SyncTable::tableCreate();
    }

    public function UnInstallDB() {
        ImportTable::tableDrop();
        SyncTable::tableDrop();
    }

}
