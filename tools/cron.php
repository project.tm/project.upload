<?php

define('IS_DEBUG', true);
$_SERVER["DOCUMENT_ROOT"] = realpath(__DIR__ . "/../../../../");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (Bitrix\Main\Loader::includeModule('project.upload')) {
    pre(Project\Upload\Event\Router::process());
    echo '<meta http-equiv="refresh" content="5;http://z-tyre.ru/local/modules/project.upload/tools/shell.php">';
}