<?php

namespace Project\Upload;

class Shell {

    const SH = '/usr/bin/php5 -c /var/www/admin/php-bin/php.v2.ini -f ';

    static public function run($file) {
        echo shell_exec(self::SH . $file);
    }

}
