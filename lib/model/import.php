<?php

namespace Project\Upload\Model;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main;

class ImportTable extends DataManager {

    public static function tableCreate() {
        static::getEntity()->getConnection()->query("CREATE TABLE " . self::getTableName() . " (
            ID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
            TYPE VARCHAR(255),
            PAGE VARCHAR(255),
            IBLOCK_ID INT,
            CODE VARCHAR(255),
            PRICE INT,
            QUANTITY INT
        );");
        static::getEntity()->getConnection()->query("ALTER TABLE `" . self::getTableName() . "` ADD UNIQUE(`TYPE`, `PAGE`, `IBLOCK_ID`, `CODE`);");
        static::getEntity()->getConnection()->query("ALTER TABLE `" . self::getTableName() . "` ADD INDEX(`TYPE`);");
    }

    public static function tableDrop() {
        static::getEntity()->getConnection()->query("DROP TABLE IF EXISTS " . self::getTableName() . ";");
    }

    public static function tableClearType($type) {
//        pre("DELETE FROM " . self::getTableName() . " WHERE TYPE='" . addslashes($type) . "';");
        static::getEntity()->getConnection()->query("DELETE FROM " . self::getTableName() . " WHERE TYPE='" . addslashes($type) . "';");
    }

    public static function tableTruncate() {
        static::getEntity()->getConnection()->query("TRUNCATE " . self::getTableName() . ";");
    }

    /**
     * {@inheritdoc}
     */
    public static function getTableName() {
        return 'd_project_upload_import';
    }

    /**
     * {@inheritdoc}
     */
    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
                    )),
            new Main\Entity\StringField('TYPE'),
            new Main\Entity\StringField('PAGE'),
            new Main\Entity\IntegerField('IBLOCK_ID'),
            new Main\Entity\StringField('CODE'),
            new Main\Entity\FloatField('PRICE'),
            new Main\Entity\IntegerField('QUANTITY')
        );
    }

    public static function add(array $arData) {
        $rsData = self::getList(array(
                    'select' => array('ID'),
                    'filter' => array(
                        '=TYPE' => $arData['TYPE'],
                        '=PAGE' => $arData['PAGE'],
                        '=CODE' => $arData['CODE']
                    ),
        ));
        if ($arItem = $rsData->Fetch()) {
            self::update($arItem['ID'], $arData);
        } else {
            parent::add($arData);
        }
    }

}
