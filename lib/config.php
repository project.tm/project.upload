<?php

namespace Project\Upload;

use Bitrix\Main\Loader,
    Apstydio\Ztyre\Iblock;

Loader::includeModule('apstydio.ztyre');

class Config {

    const IS_DEBUG = true;
    const MODULE = 'project.upload';
    const CATALOG_TIRES_ID = Iblock\Config::CATALOG_TIRES_ID;
    const OFFERS_WHEELS_ID = Iblock\Config::OFFERS_WHEELS_ID;
    const OFFERS_ACCUMULATORS_ID = Iblock\Config::OFFERS_ACCUMULATORS_ID;
    const OFFERS_EXPANDABLES_ID = Iblock\Config::OFFERS_EXPANDABLES_ID;
    const PRICE_ID = 1;
    const LIMIT = 30;
    const AGENT = array(
        'pwrs.tires'=>'\Project\Upload\Agent\Pwrs\Tires',
        'sync'=>'\Project\Upload\Agent\Sync',
    );

}
