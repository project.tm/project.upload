<?php

namespace Project\Upload\Search\Model;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main;

class Iblock4Table extends DataManager {

    /**
     * {@inheritdoc}
     */
    public static function getTableName() {
        return 'b_iblock_element_prop_s4';
    }

    /**
     * {@inheritdoc}
     */
    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('IBLOCK_ELEMENT_ID', array(
                'primary' => true,
                'autocomplete' => true
                    )),
            new Main\Entity\StringField('CML2_ARTICLE', array(
                'column_name' => 'PROPERTY_55',
                    )),
        );
    }

}
