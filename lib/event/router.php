<?php

namespace Project\Upload\Event;

use Project\Upload\Config,
    Project\Upload\Settings;

class Router {

    const AGENT_TIME = 5 * 60;
    const ROUTER_TIME = 5 * 60;
    const DAY_TIME = 24 * 60 * 60;

    static public function process() {
        try {
            set_time_limit(0);
            if (true) {
                foreach (Config::AGENT as $key => $value) {
                    pre($key, date('H:i:s', Settings::get($key)));
                }
            }
            if (self::isStart()) {
                $type = Settings::get(__CLASS__);
                if ($type and empty(Config::AGENT[$type])) {
                    $type = false;
                }
                if (empty($type)) {
                    foreach (Config::AGENT as $key => $value) {
                        $time = Settings::get($key);
                        if ($time and ( $time + self::DAY_TIME) > time()) {
                            continue;
                        }
                        $type = $key;
                        break;
                    }
                }
                if ($type) {
                    $agent = Config::AGENT[$type];
                    if ($agent::process($type)) {
                        Settings::set(__CLASS__, $agent);
                    } else {
                        Settings::set($type, time());
                        Settings::clear(__CLASS__);
                    }
                }
                self::stop();
            }
        } catch (Exception $ex) {
            echo ($ex->getMessage());
            pre($exc->getTraceAsString());
        }
        return '\Project\Upload\Agent::router();';
    }

    static private function isStart() {
        return true;
        $is = Settings::get('start');
        if ($is and ( $is + self::ROUTER_TIME) > time()) {
            return false;
        }
        self::next();
        return true;
    }

    static private function next() {
        Settings::set('start', time());
    }

    static private function stop() {
        Settings::clear('start');
    }

}
