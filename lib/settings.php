<?php

namespace Project\Upload;

class Settings {

    static private function getPath($path) {
//        pre($path);
        $path = $_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/' . Config::MODULE . '/settings/' . sha1($path);
//        pre($path);
        CheckDirPath($path);
        return $path;
    }

    static public function get($path, $default = '') {
        $path = self::getPath($path);
        return file_exists($path) ? file_get_contents($path) : $default;
    }

    static public function clear($path) {
        unlink(self::getPath($path));
    }

    static public function set($path, $data) {
        file_put_contents(self::getPath($path), $data);
    }

}
