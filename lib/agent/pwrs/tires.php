<?php

namespace Project\Upload\Agent\Pwrs;

use Cutil,
    Project\Upload\Traits,
    Project\Upload\Settings,
    Project\Upload\Config,
    Project\Upload\Utility,
    Project\Upload\Model\ImportTable;

class Tires {

    use Traits\Agent;
    use Traits\Json;
    use Traits\Section;

    const LIMIT = 50;
    const IBLOCK_ID = Config::CATALOG_TIRES_ID;
    const FILE = 'https://b2b.pwrs.ru/export_data/M13820.json';

    static protected function cleateImport($type) {
        ImportTable::tableClearType(__CLASS__);
    }

    static protected function importData($arData) {
//        pre($arData);


        $arParams = array("replace_space" => "-", "replace_other" => "-");
        $arFields = array(
            'DATE_ACTIVE_FROM' => ConvertTimeStamp(time(), 'FULL'),
            'TIMESTAMP_X' => ConvertTimeStamp(time(), 'FULL'),
            'DATE_CREATE' => ConvertTimeStamp(time(), 'FULL'),
            'IBLOCK_ID' => self::IBLOCK_ID,
            'IBLOCK_SECTION_ID' => self::getSection(self::IBLOCK_ID, $arData->brand, $arData->model),
            'NAME' => $arData->name,
            'SORT' => '500',
            'ACTIVE' => 'Y',
            'CODE' => Cutil::translit($arData->cae, "ru", $arParams),
            'DETAIL_TEXT' => '',
            'DETAIL_TEXT_TYPE' => 'html',
            'PREVIEW_TEXT' => '',
            'PREVIEW_TEXT_TYPE' => 'html',
        );
        $propFields = array(
            'SHIRINA_PROFILYA' => $arData->width,
            'VYSOTA_PROFILYA' => $arData->height,
            'INDEKS_NAGRUZKI' => $arData->load_index,
            'INDEKS_SKOROSTI' => $arData->speed_index,
            'PROIZVODITEL' => $arData->brand,
            'CML2_ARTICLE' => $arData->cae,
            'MODEL_AVTOSHINY' => $arData->model,
            'SEZONNOST' => Utility\Property::props(self::IBLOCK_ID, 67, $arData->season),
            'TIP_AVTOSHINY' => $arData->tiretype,
            'KONSTRUKTSIYA_AVTOSHINY' => $arData->design,
            'SHIPY' => $arData->thorn_type ? 19 : '',
            'MODEL_AVTOSHINY' => $arData->model,
        );

        $arFiter = array(
            'IBLOCK_ID' => $arFields['IBLOCK_ID'],
            'NAME' => $arFields['NAME'],
            'PROPERTY_PROIZVODITEL' => $propFields['PROIZVODITEL'],
        );
        $arItem = Utility\Catalog::searchByFilter($arFiter, $arFields, $propFields);

        $arFields = $propFields = array();
        if ($arData->img_big_pish) {
            $img = $arData->img_big_pish;
            if (empty($arItem['DETAIL_PICTURE']) and ! empty($img)) {
                $domen = 'http://4tochki.ru/';
                $img = substr($img, strlen($domen));
                if ($arFile = Utility\Image::upload($img, $domen)) {
                    $arFields["DETAIL_PICTURE"] = $arFile;
                }
            }
        }

        Utility\Catalog::update($arItem, $arFields, $propFields);

        $index = 0;
        $type = $key;
        foreach ($arData as $key => $value) {
            $index++;
            if ($index == 2) {
                $type = substr($key, 6);
                break;
            }
        }

        $rozn = 'price_' . $type . '_rozn';
        $quality = 'rest_' . $type;
        Utility\Catalog::saveCatalog($arItem);
        if ($type == 'mkrs') {
            Utility\Catalog::savePrice($arItem, $arData->price, 'RUB');
//            preExit($arItem, $type, $arData->price);
        }

        ImportTable::add(array(
            'TYPE' => __CLASS__,
            'PAGE' => $type,
            'IBLOCK_ID' => self::IBLOCK_ID,
            'CODE' => $arItem['ID'],
//            'PRICE' => $item['Розница (руб.)'],
            'QUANTITY' => $arData->{$quality},
        ));
//        preExit($arItem, $arFields, $propFields);
    }

}
