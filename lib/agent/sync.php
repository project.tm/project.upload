<?php

namespace Project\Upload\Agent;

use Bitrix\Main\Application,
    Project\Upload\Traits,
    Project\Upload\Settings,
    Project\Upload\Config,
    Project\Upload\Utility,
    Project\Upload\Model\SyncTable,
    Project\Upload\Model\ImportTable;

class Sync {

    const LIMIT = 500;

    use Traits\Agent;

    static protected function cleateImport($type) {

    }

    static public function processPage($page) {
        if ($page==1) {
            SyncTable::tableTruncate();
            Application::getConnection()->query('INSERT ' . SyncTable::getTableName() . ' SELECT IBLOCK_ID, CODE, COUNT(QUANTITY) AS QUANTITY FROM ' . ImportTable::getTableName() . ' GROUP BY IBLOCK_ID, CODE;');
        }
        $start = $page * self::LIMIT;
        $end = ($page + 1) * self::LIMIT;
        pre($page);

        $rsData = Application::getConnection()->query('SELECT SQL_CALC_FOUND_ROWS IBLOCK_ID, CODE, QUANTITY FROM ' . SyncTable::getTableName() . ' LIMIT ' . $start . ', ' . self::LIMIT);
        $count = Application::getConnection()->queryScalar('SELECT FOUND_ROWS() as TOTAL');
        $pageIsNext = (self::LIMIT * ($page + 1)) < $count;

        while ($arData = $rsData->Fetch()) {
            self::importItem($arData);
        }
        return $pageIsNext ? self::next() : self::stop();
    }

    protected function importItem($arData) {
        $arFiter = array(
            'IBLOCK_ID' => $arData['IBLOCK_ID'],
            'ID' => $arData['CODE'],
        );
        $arItem = Utility\Catalog::searchByFilter($arFiter, $arFields, $propFields);
        Utility\Catalog::saveCatalog($arItem, false, $arData['QUANTITY']);
    }

}
