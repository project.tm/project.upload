<?php

namespace Project\Upload\Traits;

use Project\Core\Utility,
    Project\Upload\Config,
    Project\Upload\Settings;

trait Agent {

    static private $type = '';
    static private $page = '';

    static protected function cleateImport($type) {
        throw new \Exception('Не создана функция очистки импорта');
    }

    static public function process($type) {
        self::$type = $type;
        self::$page = (int) Settings::get(self::$type, 1);
        pre('$page', self::$page);
        if (self::$page == 1) {
            static::cleateImport($type);
        }
        if(static::processPage(self::$page)) {
            return self::next();
        } else {
            return self::stop();
        }
    }

    static protected function next() {
        Settings::set(self::$type, ++self::$page);
        return true;
    }

    static protected function stop() {
        Settings::clear(self::$type);
        return false;
    }

    static protected function getPath($path) {
        $path = $_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/' . Config::MODULE . '/agent/' . sha1($path);
        CheckDirPath($path);
        return $path;
    }

    static protected function upload($file, $isCache = false) {
        pre($file, $isCache);
        if (empty($isCache)) {
            file_put_contents(self::getPath($file), file_get_contents($file));
        }
        return self::getPath($file);
    }

}
