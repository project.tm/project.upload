<?php

namespace Project\Upload\Traits;

use SimpleXMLElement,
    cFile,
    Project\Import\Parse\Records,
    Project\Log,
    Project\Import\Data,
    Project\Import\Config;

trait Csv {

    abstract protected function getFile();

    static public function processPage($page) {
//        $GLOBALS['APPLICATION']->RestartBuffer();
//        if ($page == 1) {
//            Records::clear();
//        }
        $limit = Config::LIMIT;
        $start = ($page - 1) * $limit;
        $end = ($page) * $limit;

        $filename = static::getFiles();
        if (file_exists($filename)) {
            $filelen = strlen(file_get_contents($filename));
            $key = -1;
//            echo '<h3>Выполнено ' . round(($page - 1) / ceil(count($arData) / $limit) * 100, 2) . '% (' . $start . '/' . count($arData) . ')</h3>';
            echo '<h3>Разобрано ' . ($page - 1) * $limit . ' строк</h3>';
            set_time_limit(3600);
            if (($handle = fopen($filename, "r")) !== FALSE) {
//                pre($handle);
                while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {
                    $key++;
                    if ($key < $start) {
                        continue;
                    }
                    if ($key >= $end) {
                        return true;
                    }
                    if (empty($key) or empty($data)) {
                        continue;
                    }
                    preExit($data);
                    $arData = array_map(function($value) {
                        return iconv('CP1251', "UTF-8", $value);
                    }, $data);
                    static::importProduct($arData);
//                    exit;
                };
                fclose($handle);
            }
        }
        return false;
    }

}
