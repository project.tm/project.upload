<?php

namespace Project\Upload\Traits;

use Cutil,
    CIBlockSection;

trait Section {

    static protected function getSection($iblockId, $section, $subsection = '') {
        static $arSection = array();
        static $arCode = array();
        static $arSubSection = array();

        if (empty($arSection)) {
            $tree = CIBlockSection::GetTreeList(
                            array('IBLOCK_ID' => $iblockId), array('ID', 'CODE','IBLOCK_SECTION_ID', 'DEPTH_LEVEL', 'NAME')
            );
            while ($arItem = $tree->Fetch()) {
                $arCode[$arItem['ID']] = $arItem['CODE'];
                switch ($arItem['DEPTH_LEVEL']) {
                    case 1:
                        $arSection[$iblockId][strtolower($arItem['NAME'])] = $arItem['ID'];
                        break;

                    case 2:
                        $arSubSection[$iblockId][$arItem['IBLOCK_SECTION_ID']][strtolower($arItem['NAME'])] = $arItem['ID'];
                        break;

                    default:
                        break;
                }
            }
        }
        $sectionLower = strtolower($section);
        $subsectionLower = strtolower($subsection);

        $bs = new CIBlockSection;
        $arParams = array("replace_space" => "-", "replace_other" => "-");
        if (isset($arSection[$iblockId][$sectionLower])) {
            $sectionId = $arSection[$iblockId][$sectionLower];
        } else {
            $arFields = Array(
                "ACTIVE" => 'Y',
                "IBLOCK_SECTION_ID" => 0,
                "IBLOCK_ID" => $iblockId,
                "NAME" => $section,
                'CODE' => Cutil::translit($section, "ru", $arParams),
                "SORT" => 500,
            );
//            pre($arFields);
            $sectionId = $arSection[$iblockId][$sectionLower] = $bs->Add($arFields);
            if (empty($sectionId)) {
                echo $bs->LAST_ERROR;
            }
        }

        if(empty($subsection)) {
            return $sectionId;
        }

        if (isset($arSubSection[$iblockId][$sectionId][$subsectionLower])) {
            $subsectionId = $arSubSection[$iblockId][$sectionId][$subsectionLower];
        } else {
            $arFields = Array(
                "ACTIVE" => 'Y',
                "IBLOCK_SECTION_ID" => $sectionId,
                "IBLOCK_ID" => $iblockId,
                "NAME" => $subsection,
                'CODE' => Cutil::translit($subsection, "ru", $arParams),
                "SORT" => 500,
            );
//            pre($arFields);
            $subsectionId = $arSubSection[$iblockId][$sectionId][$subsectionLower] = $bs->Add($arFields);
            if (empty($subsectionId)) {
                echo $bs->LAST_ERROR;
            }
        }
        return $subsectionId;
    }

}
